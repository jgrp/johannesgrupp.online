export const projects = [
  {
    title: 'Practice for Logopedics',
    img: 'https://www.johannesgrupp.online/fileadmin/development/web-logopaedie-nuxt_01.jpg',
    task: 'Website',
  }, {
    title: 'Flutter Project',
    img: 'https://www.johannesgrupp.online/fileadmin/_processed_/3/2/csm_web-flutter_aa42d6d221.jpg',
    task: 'App',
  }, {
    title: 'Info sheet',
    img: 'https://www.johannesgrupp.online/fileadmin/_processed_/8/c/csm_web-1stcut_6fe7efc89b.jpg',
    task: 'Handout',
  }
];

export const certs = [
  {
    name: 'Angular Certificate',
    desc: 'HackerRank Skill Certification (Assessment) • 2021',
    link: 'https://www.hackerrank.com/certificates/217569843062'
  }, {
    name: 'JavaScript Certificate',
    desc: 'HackerRank Skill Certification (Assessment) • 2021',
    link: 'https://www.hackerrank.com/certificates/96812d5e34ac'
  }, {
    name: 'TYPO3 CMS Certified Integrator (TCCI)',
    desc: 'TYPO3 Association • 2018',
    link: 'https://typo3.com/services/certifications/integrator-certification'
  }, {
    name: 'Boost your TYPO3 frontend with RequireJS',
    desc: 'Publication on pagemachine.de/blog • 2018',
    link: 'https://pagemachine.de/blog/typo3-frontend-requirejs/'
  }
];
