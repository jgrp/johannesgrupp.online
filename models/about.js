export const skills = [
  {
    title: 'Top Skills',
    content: ['JavaScript', 'Angular', 'Vue.js', 'Git']
  }, {
    title: 'Technical Skills',
    content: ['JavaScript', 'Angular', 'Vue.js', 'Backbone.js', 'HTML5', 'Css3', 'TypeScript', 'Less', 'Sass', 'Git', 'Typo3']
  }, {
    title: 'Soft Skills',
    content: '<p class="mb-3">Experienced with people management topics and leading roles</p>' +
     '<p class="mb-3">Explaining complex technical details to non-technical customers</p>' +
      '<p class="mb-3">Empathy</p><p class="mb-3">Mentoring of fellow engineers</p><p class="mb-3">Committed to learning new technologies</p>'
  }, {
    title: 'Technologies',
    content: [
       'Nx Workspace', 'Storybook', 'NgRx', 'Observables', 'RxJS', 'Jasmin', 'Karma', 'RequireJS', 'Underscore.js', 'Lodash', 'NPM', 'Composer', 'Ajax', 'Grunt', 'Webpack', 'REST',
      'Twitter Bootstrap', 'Material Components', 'Material-UI', 'Vuetify', 'JSS', 'JSX', 'Handlebars.js', 'Fluid',
    ]
  }, {
    title: 'Tools',
    content: [
      'PhpStorm', 'Sublime 3', 'Bitbucket', 'Gitlab', 'Jira', 'Adobe Photoshop', 'Adobe Premiere', 'Adobe Illustrator'
    ]
  }, {
    title: 'Personal',
    content: ['Flutter', 'Nuxt.Js']
  }
];
