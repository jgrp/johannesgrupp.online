const nav = [{
    link: '/',
    title: '.init()'
  } , {
    link: '/about',
    title: '.about()'
  } , {
    link: '/projects',
    title: ".projects()"
}];

export default nav;
