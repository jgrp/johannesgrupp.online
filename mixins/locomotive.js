export default {
  methods: {
    initLocomotive() {
      new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true,
        offset: ['10%','10%']
      });
    }
  }
}
